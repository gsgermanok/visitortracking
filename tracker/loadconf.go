package loadconf

import (
  "path"
  "errors"
  "visitortracking/lib/config"
)  

var (
  ConfPaths []string
)

type MergedConfig struct {
        config  *config.Config
        section string // Check this section first, then fall back to DEFAULT
}

func load(confName string) (*MergedConfig, error) {
    var err error
    for _, confPath := range ConfPaths {
        conf, err := config.ReadDefault(path.Join(confPath, confName))
        if err == nil {
            return &MergedConfig{conf, ""}, nil
        }
    }
    if err == nil {
        err = errors.New("not found")
    }
    return nil, err
}

