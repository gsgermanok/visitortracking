package main

import (
  "fmt"
  "net/http"
  "os"
  "path"
  "errors"
  "time"
  "visitortracking/lib/config"
)

type tracking struct {
  remoteAddr string
  userAgent string
  referer string
}

type history []tracking

type visitor struct {
  id uint
  ip string
  counter uint
  cookie bool 
  tracker *history
  firstVisit time.Time
  lastVisit time.Time
}

type MergedConfig struct {
        config  *config.Config
        section string // Check this section first, then fall back to DEFAULT
}

var (
  ConfPaths []string
)


func load(confName string) (*MergedConfig, error) {
    var err error
    for _, confPath := range ConfPaths {
        conf, err := config.ReadDefault(path.Join(confPath, confName))
        if err == nil {
            return &MergedConfig{conf, ""}, nil
        }
    }
    if err == nil {
        err = errors.New("not found")
    }
    return nil, err
}

func (c *MergedConfig) String(option string) (result string, found bool) {                                    
  if r, err := c.config.String(c.section, option); err == nil {                                               
    return stripQuotes(r), true                                                                               
  }                                                                                                           
  return "", false                                                                                            
}

// Helpers                                                                                                    
                                                                                                              
func stripQuotes(s string) string {                                                                           
  if s == "" {                                                                                                
    return s                                                                                                  
  }                                                                                                           
  if s[0] == '"' && s[len(s)-1] == '"' {                                                                      
    return s[1 : len(s)-1]                                                                                    
  }                                                                                                           
  return s                                                                                                    
} 

func cookies(w http.ResponseWriter, r *http.Request) {
  name := "telcodepot"
  url := "http://www.telcodepot.com"
  cookie, err := r.Cookie(name)
  if err != nil {
    expiration := time.Now().Add(365 * 24 * time.Hour)
    newcookie := http.Cookie{Name: name, Value: "{id: 1333, counter: 1, url: "+url+", lang: "+r.Header["Accept-Language"][0]+"}", Expires: expiration}
    http.SetCookie(w, &newcookie)
  } else {
    fmt.Fprint(w, "### ", cookie.Name, " / ", cookie.Value, " / ", cookie.Expires, " / ",cookie.Domain)
    for _, cookie := range r.Cookies() {
      fmt.Fprint(w, "### ", cookie.Name, " / ", cookie.Value, " / ", cookie.Expires, " / ",cookie.Domain)
    }
  }
}

func server(w http.ResponseWriter, req *http.Request) {
  config, err := load("app.conf")
  check(err)
  if r, found := config.String("database.user"); found {
    fmt.Println(r, "; ",found)
  }
  cookies(w, req)
}

func main() {
  ConfPaths = []string{"/home/ggerman/go/src/visitortracking"}
  port := "8181"
  intro(port)
 
  http.HandleFunc("/", server)
  err := http.ListenAndServe("localhost:" + port, nil)
  check(err)
}

func check(err error) {
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
}

func intro(port string){
  fmt.Println("###############################################")
  fmt.Println("# start server in PORT: ",port , "                #")
  fmt.Println("###############################################")
  fmt.Println("# ggerman@gmail.com                           #")
  fmt.Println("###############################################")
  fmt.Print("> ")
}

